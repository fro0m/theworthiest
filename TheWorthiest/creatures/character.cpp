#include "character.h"

using std::string;

int Character::nCharacters_ = 0;

//Character::Character() {nCharacters_++;}

Character::~Character()
{

}

Character::Character(const std::string& _name, int _level) : Creature( _name, _level) {
    nCharacters_++;
}

int Character::getHp() const
{
    return stats_.hp;
}

int Character::takeDamage(int _damage)
{
    stats_.hp -= _damage;
    if ( stats_.hp <= 0) {
        dead_ = 1;
        stats_.hp = 0;
    }
    return stats_.hp;
}



