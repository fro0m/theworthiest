#ifndef DATA
#define DATA
#include <string>
enum Color {RED, GREEN, BLUE};
enum ItemType {INVENTORY, HEAD, CHEST, LEGS, GLOVES, BOOTS, BELT, CLOAK, LEFT_HAND, RIGHT_HAND,
               TW0_HAND, BOTH_HAND, LEFT_EAR, RIGHT_EAR, NECKLACE, BRACLET};

typedef int Chant;
typedef int Reinforcement;

/*---------------------------------------------------------------------------------------
  Consist of ids of goods from database weared by a character.
  id is a shift of index in array of goods. C++11 style default initialization.
---------------------------------------------------------------------------------------*/
struct Equipment
{
    int head = 0;
    int chest = 1;// default. Shirt.
    int legs = 1;// default. Dirty pants with holes.
    int gloves = 0;
    int boots = 0;
    int belt = 0;
    int cloak = 0;

    int left_hand = 0;
    int right_hand = 1;// Stick.

    int left_ear = 0;
    int right_ear = 0;
    int necklace = 0;
    int braclet = 0;
};

/*-------------------------------------------------------------------------------
  List of all stats. C++11 style default initialization.
-------------------------------------------------------------------------------*/
struct Stats
{
    int pdef = 1;        // Physical defence constant.
    int mdef = 0;        // Magical defence constant.
    int patk = 2;        // Physical attack constant.
    int matk = 0;        // Magical attack constant.
    int acc = 10;        // Accuracy constant.
    int eva = 10;        // Evasion constant.
    int crit = 0;        // Critical rating constant.

    int str = 10;            // Strength constant. //P. Atk., P. Critical Damage.
    int intel = 10;          // Intellect constant. //M. Atk., M. Critical Damage
    int dex = 10;            // Dexterity constant. //P. Critical Rate, P. Accuracy, P. Evasion, Atk. Spd.
    int wis = 10;            // Wisdom constant //M. Critical Rate, M. Accuracy, M. Evasion, Casting Spd., M. Def. Rate
    int con = 10;            // Constitution const. //Max HP, HP Regeneration, Weight, Breath, Shield Def. Success Rate,

    int hp = 50;             // Current health points of a character.
    int mana = 10;           // Current mana points of a character.
};

struct Slot
{
    Color slotColor;
    int slotAdr = 0;//address of a slot in database. default null-slot item (no item inserted)
};
#endif // DATA

