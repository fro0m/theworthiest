#include "itemwearable.h"

ItemWearable::ItemWearable(Reinforcement _reinforcement, Color* _slotColors, int _nSlot,
                           std::string _name, ItemType _itemType, int _nChant)
    : Item( _name, _itemType, _nChant)
{
    reinforcement_ = _reinforcement;
    nSlot_ = _nSlot;
    if (_nSlot > 0 && _nSlot <= MAXNSLOT) {
        slotS_ = new Slot[ _nSlot];
        for (int i = 0; i < _nSlot; i++)
        {
            slotS_[i].slotColor = _slotColors[i];
        }
    }
}

ItemWearable::~ItemWearable()
{
    delete [] slotS_;
}

ItemWearable::ItemWearable(const ItemWearable& _this)
    : Item( _this)
{
    reinforcement_ = _this.reinforcement_;
    nSlot_ = _this.nSlot_;
    if (nSlot_ > 0) {
        slotS_ = new Slot[ nSlot_];
        for (int i = 0; i < nSlot_; i++)
        {
            slotS_[i].slotColor = _this.slotS_[i].slotColor;
            slotS_[i].slotAdr = _this.slotS_[i].slotAdr;
        }
    }
}

ItemWearable& ItemWearable::operator=(const ItemWearable &_this)
{
    if (this == &_this)
        return *this;
    Item::operator =( _this);
    nSlot_ = _this.nSlot_;
    delete [] slotS_;
    slotS_ = new Slot[ nSlot_];
    reinforcement_ = _this.reinforcement_;
    return *this;
}

