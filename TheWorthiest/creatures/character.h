#ifndef CHARACTER_H
#define CHARACTER_H
#include "creature.h"
//#include "monster.h"
#include "data.h"

/*--------------------------------------------------------
  all character's characteristics, states, actions.
--------------------------------------------------------*/
class Character : public Creature
{
private:
    int experience_ = 0;     // Current experience points.
    Equipment equipment_;    // Stuff dressed on a character.
    Stats stats_;
    static int nCharacters_;// All the characters for the statistics.


public:
    //Character();
    ~Character();

    Character(const std::string& _name = "", int _level = 0);

    int getHp() const;
    int getMp() const;
    int takeDamage(int _damage);// Causing damage to the character. it returns hd after damage.
    int takeHeal(int _heal);// Causing heal to the character. it returns hp after heal.

    //friend int fight(const Character& character, const Monster& monster); //return status code

};

#endif // CHARACTERSTATE_H
