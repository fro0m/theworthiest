#include "creature.h"

/*------------------------------------------------------------------------------
  Base class with common data
------------------------------------------------------------------------------*/

Creature::~Creature()
{

}

const std::string& Creature::getName() const
{
    return name_;
}

int Creature::getLvl() const
{
    return level_;
}
