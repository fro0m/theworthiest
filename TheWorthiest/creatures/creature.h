#ifndef CREATURE_H
#define CREATURE_H
#include "data.h"


/*------------------------------------------------------------------------------
  The base class for creatures
------------------------------------------------------------------------------*/
class Creature
{
protected:
    std::string name_;        // Name of a creature.
    int level_;          // Current level of creature.
    bool dead_ = 0;          // State of a character: dead or alive. dead when 1.



public:
    //Creature();
    Creature(const std::string& _name = "", const int _level = 0) : name_( _name), level_( _level) {}
    virtual ~Creature();
    const std::string& getName() const;
    int getLvl() const;
};

#endif // CREATURE_H
