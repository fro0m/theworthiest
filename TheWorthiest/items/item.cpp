#include "item.h"
#include <string>



Item::Item(std::string _name, ItemType _itemType, int _nChant)
    : name_( _name), itemType_( _itemType), nChant_( _nChant)
{
    if (_nChant > 0 && _nChant <= MAXNCHANT) {
        chantS_ = new Chant[ _nChant];
    }
}

Item::~Item()
{
    delete [] chantS_;
}

Item::Item(const Item& _this)
{
    nChant_ = _this.nChant_;
    if ( nChant_ > 0 && nChant_ <= MAXNCHANT)
    {
        chantS_ = new Chant[ nChant_];
        for (int i = 0; i < nChant_; i++)
        {
            chantS_[i] = _this.chantS_[i];
        }
    }
    name_ = _this.name_;
    itemType_ = _this.itemType_;
}

Item& Item::operator=(const Item& _this)
{
    if (this == &_this)
        return *this;
    delete [] chantS_;
    nChant_ = _this.nChant_;
    chantS_ = new Chant[ nChant_];
    name_ = _this.name_;
    itemType_ = _this.itemType_;
    return *this;
}

