#ifndef ITEMWEARABLE_H
#define ITEMWEARABLE_H
#include "data.h"
#include "item.h"


class ItemWearable : public Item
{
private:
    Reinforcement reinforcement_;
    Slot* slotS_ = nullptr;
    int nSlot_;

public:
    ItemWearable(Reinforcement _reinforcement = 0, Color* _slotColors = nullptr, int _nSlot = 0,
                 std::string _name = "", ItemType _itemType = INVENTORY, int _nChant = 0);
    ~ItemWearable();
    ItemWearable(const ItemWearable& _this);
    ItemWearable& operator=(const ItemWearable& _this);
};

#endif // ITEMWEARABLE_H
