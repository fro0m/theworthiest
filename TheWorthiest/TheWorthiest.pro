TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11


SOURCES += main.cpp \
    creatures/character.cpp \
    creatures/creature.cpp \
    creatures/monster.cpp \
    items/item.cpp \
    items/itemwearable.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    creatures/character.h \
    creatures/monster.h \
    items/item.h \
    main.h \
    data.h \
    creatures/creature.h \
    items/itemwearable.h

INCLUDEPATH += . $$PWD/creatures $$PWD/items
    .

# todo
#   +make a friend fuctions
#
