#ifndef ITEM_H
#define ITEM_H
#include <string>
#include "data.h"

const int MAXNSLOT = 4, MAXNCHANT = 5;

/*------------------------------------------------------------------------------
  The base item class
------------------------------------------------------------------------------*/
class Item
{
private:
    std::string name_;
    ItemType itemType_;
    Chant* chantS_ = nullptr; //for no op "delete [] chantS_;"
    int nChant_;
public:
    Item(std::string _name = "", ItemType _itemType = INVENTORY, int _nChant = 0);
    virtual ~Item();

    Item(const Item& _this);
    Item& operator=(const Item& _this);
};

#endif // ITEM_H
