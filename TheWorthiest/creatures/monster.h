#ifndef MONSTER_H
#define MONSTER_H
#include "creature.h"
//#include "character.h"
#include "data.h"

class Monster : public Creature
{
    bool boss_ = 0;
    Stats stats_;
public:
    Monster();
    ~Monster();

    bool isBoss() const { return boss_;}

    //friend int fight(const Character& character, const Monster& monster); //return status code
};

#endif // MONSTER_H
