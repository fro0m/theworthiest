#ifndef SLOT_H
#define SLOT_H
#include "data.h"
#include "item.h"



class Slot : public Item
{
private:
    struct SlotStructure
    {
        Color slotColor;
        bool inserted = 0;
        int slotAdr;//address of a slot in darabase
    };

public:
    Slot();
    ~Slot();
};

#endif // SLOT_H
